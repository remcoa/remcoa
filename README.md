# remcoa

public README
<rawhtml>
<table align=left style="min-width: 640px; border-top: 1px solid #CCCCCC; border-bottom: 1px solid #CCCCCC; padding-left:0px; padding-right: 0px; font-family: Arial, Helvetica, 'sans-serif';" cellspacing=0 cellpadding=0 align=center border=0>
<tr>
 <td style='visibility: hidden; color: #FFFFFF; font-size: 1px;'>Radboud Universiteit Nijmegen</td>
 <td rowspan=2 width=334><img align=right src="/cncz/images/f/fe/Remcoaalbers.jpg" height=150 border=0 alt=''><a href='http://www.ru.nl/'><img src="http://www.cncz.science.ru.nl/images/ru-logo_signature.gif" width=334 height=150 border=0 alt=''></a></td>
</tr>
<tr>
 <td>
  <table width='100%' cellspacing=0 cellpadding=2>
  <tr>
   <td nowrap colspan=2 style='font-size: 16px;'><a style='color: black; text-decoration: none;' href='http://www.science.ru.nl/'>Faculteit der Natuurwetenschappen,<br>Wiskunde en Informatica&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</a></td>
  </tr>
  <tr>
   <td nowrap colspan=2 style='font-size: 14px; padding-bottom: 8px; padding-top: 0px;'>- <a style='color: black; text-decoration: none;' href='http://www.cncz.science.ru.nl/'>Computer- &amp; Communicatiezaken</a></td>
  </tr>
  <tr>
   <td colspan=2 style='font-size: 14px; font-weight: bold; padding-bottom:0px;'>Drs. P.A.T. (Remco) Aalbers</td>
  </tr>
  <tr>
   <td colspan=2 style='font-size: 10px; padding-bottom:10px;'>(web) Applicatie- en systeemontwikkelaar<br>Internet en open source specialist<br></td>
  </tr>
  <tr>
   <td width='60%' valign=top style="padding-bottom:6px;">
    <table width='100%' cellspacing=0 cellpadding=0>
    <tr>
     <td style='font-size: 10px; font-weight: bold;'>Bezoekadres</td>
    </tr>
    <tr>
     <td nowrap style='font-size: 10px; padding-bottom:0px;'>Heyendaalseweg 135, HG03.055<br><b>T</b>&nbsp;<span style='padding-left:3px;'>0031 24 36 52746</span></td>
    </tr>
    <tr>
     <td style='font-size: 10px;'><b>E</b></span>&nbsp;<span style='padding-left:3px;'>R.Aalbers@science.ru.nl</a></span></td>
    </tr>
    <tr>
     <td style='font-size: 10px;'><b>W</b>&nbsp;<span><a href='http://wiki.science.ru.nl/cncz/User:Remcoa'>Website</a></span></td>
    </tr>
    </table>
   </td>
   <td width='40%' valign=top>
    <table width='100%' cellspacing=0 cellpadding=0>
    <tr>
     <td style='font-size: 10px; font-weight: bold;'>Postadres</td>
    </tr>
    <tr>
     <td nowrap style='font-size: 10px;'>Postbus 9010<br>6500 GL Nijmegen!</td>
    </tr>
    </table>
   </td>
  </tr>
  </table>
 </td>
</tr>
</table>
<br clear='all'>
</rawhtml>


| **Applications** | **(some) Expertise in** | **Interested in**     |
| ---------------- | ----------------------- | --------------------- |
| -   ALICE        | -   Open Source        | -   Productivity/Time  |
| -   EduView     | -   (PHP) Application  |     management (GTD,   |
| -   Jive        |     development        |     Inbox Zero)        |
| -   LimeSurvey  | -   MySQL              | -   Agile Programming, |
| -   OMV         | -   Internet           |     Agile Management   |
| -   Wiki        |                        | -   datamanagement/koppeling |
